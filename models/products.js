import mongoose from 'mongoose';


let Products = new mongoose.Schema({
    id: Number,
    brand: String,
    description: String,
    image: String,
    price: Number
  })

export default mongoose.model('products', Products)