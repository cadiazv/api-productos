import express from 'express';
import bodyParser from 'body-parser';
import getProducts from '../services/getProducts.js';

let router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));


router.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');

    // authorized headers for preflight requests
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();

    router.options('*', (req, res) => {
        // allowed XHR methods  
        res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
        res.send();
    });
});

router.get('/inicio', function (req, res){
    res.send('llegamos a la pagina')
})

router.post('/products', function (req, res, next){
    
    getProducts(req.body.producto)
    .then(respon =>{
        res.send(respon);
        next();
    })
    .catch((err) => {
        res.status(500).send(err)
        next();
    })
})

export default router;