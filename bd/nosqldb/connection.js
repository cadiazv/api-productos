import mongoose from 'mongoose';

var mongoDB = 'mongodb://localhost:27017/promotions';

mongoose.connect(mongoDB,{
        "auth": { "authSource": "admin" },
        "user": "productListUser",
        "pass": "productListPassword",
        useUnifiedTopology: true,
        useNewUrlParser: true
    });


//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', function() {
    console.log('conectado mongodb');
});
 export default db;