import Products from '../models/products.js';

let isPalindrome = (word) =>{
        
    let isPalindromo = true;

    for (const i in word.toLowerCase().split('')){
        if(word.toLowerCase().split('')[i] != word.toLowerCase().split('').reverse()[i]){
            isPalindromo = false;
        }
    }

    return isPalindromo;
}

let createResult = (data, palindromoResult) => {  
    
    let result = [];

    if(data.length < 1) return(result)
    else{
        for (let index = 0; index < data.length; index++) {

            let descuento = 0;
            let element = data[index];
            let price = element.price;

            if(palindromoResult){
                price = parseInt(price/2);
                descuento = 50;
            }

            let objectResult = {
                id: element.id,
                brand: element.brand,
                description: element.description,
                image: element.image,
                price: price,
                originalPrice: element.price,
                descuento: descuento
            }
            result.push(objectResult);

            if(index+1 == data.length){
                return(result)
            }                
        }
    }
}


let getProducts = async (value) => {

    const productSearch = value;
    let productId = null;

    try {
        productId = parseInt(productSearch,10);
    } catch (error) {
        productId = searchId;
    }

    if(isNaN(productId)){
        productId = null;
    }

    try {
        let data = await Products.find({$or: [{brand: productSearch},{description: productSearch},{id: productId}]});
        let palindromoResult = isPalindrome(productSearch);
        let dataResult = createResult(data, palindromoResult);
        return dataResult
    } catch (error) {
        throw error
    }
}

export default getProducts;