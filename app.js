import express from 'express';
import bodyParser from 'body-parser';
import connection from './bd/nosqldb/connection.js';
import router from './routers/index.js';


const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// ROUTERS

app.use('/', router);

// INICIALICE SERVER

app.listen(5000, function () {
console.log('Server listening on port 5000!')
});

// ERROR

process.on('uncaughtException', function (err) {
    if (err.stack) delete err.stack;
    let error = JSON.stringify(err);
    console.error(error);
  });