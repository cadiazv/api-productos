FROM node:15.3.0 

RUN mkdir -p /usr/src/back-app

COPY . /usr/src/back-app

WORKDIR /usr/src/back-app

RUN npm install

CMD ["npm", "start"]